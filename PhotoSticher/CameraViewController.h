//
//  CameraViewController.h
//  PhotoSticher
//
//  Created by Subin on 27/09/16.
//  Copyright © 2016 qburst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>
#import <ImageIO/CGImageProperties.h>
#import <QuartzCore/QuartzCore.h>

@interface CameraViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *stichView;
@property (weak, nonatomic) IBOutlet UIImageView *imageOne;
@property (weak, nonatomic) IBOutlet UIImageView *imageTwo;
@property (weak, nonatomic) IBOutlet UIImageView *imageThree;
@property (weak, nonatomic) IBOutlet UIImageView *imageFour;

@property (nonatomic, strong) AVCaptureSession * captureSession;


@end
