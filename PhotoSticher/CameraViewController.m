//
//  CameraViewController.m
//  PhotoSticher
//
//  Created by Subin on 27/09/16.
//  Copyright © 2016 qburst. All rights reserved.
//

#import "CameraViewController.h"
#import "ImageWriterManager.h"

const static int NUMBER_OF_IMAGES = 4;
const static int TIME_INTERVAL = 5;


@interface CameraViewController (){
    
    AVCaptureStillImageOutput *stillImageOutput;
    AVCaptureVideoPreviewLayer *previewLayer;
    NSTimer *selfTimer;
    UILabel *timerCountLabel;
    int timerCount;
    UIButton *takePhotoButton;
    int capturedIndex;
}

@end

@implementation CameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self captureImage];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

   #pragma mark - Private methods
- (void)captureImage{
    
    capturedIndex = 0;
    self.captureSession = [[AVCaptureSession alloc]init];
    self.captureSession.sessionPreset = AVCaptureSessionPresetPhoto;
    
    //Add device
    AVCaptureDevice *device =[self getDeviceBackCamera];
    
    //Input
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
    
    if (!input)
    {
        [self showAlertWithTitle:@"Error" andMessage:@"Camera not suppoted on this device"];
        return;
        
    }
    
    [self.captureSession addInput:input];
    
    // Make a still image output
    stillImageOutput = [AVCaptureStillImageOutput new];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
    [stillImageOutput setOutputSettings:outputSettings];
    if ([self.captureSession canAddOutput:stillImageOutput])
        [self.captureSession addOutput:stillImageOutput];
    
    //Preview Layer
    previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    previewLayer.frame = self.view.frame;
    previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
   [self.view.layer addSublayer:previewLayer];
    //Start capture session
    [self.captureSession startRunning];
    
    
    //Add take photo btn
     takePhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [takePhotoButton setFrame:CGRectMake(self.view.center.x - 40, self.view.frame.size.height - 200 , 80, 80)];
    [takePhotoButton setImage:[UIImage imageNamed:@"camera"] forState:UIControlStateNormal];
    [takePhotoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [takePhotoButton addTarget:self action:@selector(startTimer) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:takePhotoButton];
    
    timerCountLabel = [[UILabel alloc]init];
    [timerCountLabel setFrame:CGRectMake(0, 0 , 100, 100)];
    timerCountLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:100];
    timerCountLabel.center = self.view.center;
    timerCountLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:timerCountLabel];
    timerCountLabel.hidden = YES;
    
}

- (void)resetView {
    [previewLayer removeFromSuperlayer];
    [selfTimer invalidate];
    selfTimer = nil;
    timerCountLabel.hidden = YES;
}
- (void)saveToAlbum{
    
    UIGraphicsBeginImageContext(self.stichView.frame.size);
    [self.stichView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *stichedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [[ImageWriterManager sharedManager] addImageToDocumentDirectory:stichedImage];
}

#pragma mark - timer
- (void)startTimer {

    timerCount = NUMBER_OF_IMAGES * TIME_INTERVAL;
    timerCountLabel.hidden = NO;
    timerCountLabel.text = @"5";
    takePhotoButton.hidden = YES;
    selfTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                     target:self
                                   selector:@selector(tick:)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)tick:(NSTimer *) timer {

    timerCount--;
    if (timerCount % TIME_INTERVAL == 0) {
        [self takeSnap];
    }
    timerCountLabel.text = [NSString stringWithFormat:@"%d",timerCount % TIME_INTERVAL];
    
}

  #pragma mark - image capture
-(void)takeSnap{
    
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImageOutput.connections)
    {
        for (AVCaptureInputPort *port in [connection inputPorts])
        {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection)
        { break;
        }
    }
    
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection
                                                  completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
        
         NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
         UIImage *image = [[UIImage alloc] initWithData:imageData];
         switch (capturedIndex) {
             case 0:
                 self.imageOne.image = image;
                 break;
             case 1:
                 self.imageTwo.image = image;
                 break;
             case 2:
                 self.imageThree.image = image;
                 break;
             case 3:
                 self.imageFour.image = image;
                 [self resetView];
                 [self saveToAlbum];
                 
                break;
             default:
                 break;
         }
         capturedIndex++;
         
     }];
    
}


- (void)dealloc{
    [self deallocSession];
}

-(void)deallocSession
{
    [previewLayer removeFromSuperlayer];
    for(AVCaptureInput *input in self.captureSession.inputs) {
        [self.captureSession removeInput:input];
    }
    
    for(AVCaptureOutput *output in self.captureSession.outputs) {
        [self.captureSession removeOutput:output];
    }
    [self.captureSession stopRunning];
    self.captureSession=nil;
    previewLayer=nil;
    stillImageOutput=nil;
    
}




- (AVCaptureDevice *)getDeviceBackCamera {
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == AVCaptureDevicePositionBack) {
            return device;
        }
    }
    return nil;
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    previewLayer.frame = self.view.frame;
    [takePhotoButton setFrame:CGRectMake(self.view.center.x - 40, self.view.frame.size.height - 200 , 80, 80)];
    timerCountLabel.center = self.view.center;
}
@end
