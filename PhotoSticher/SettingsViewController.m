//
//  SettingsViewController.m
//  PhotoSticher
//
//  Created by Subin on 26/09/16.
//  Copyright © 2016 qburst. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions
- (IBAction)logoutButtonClicked:(id)sender {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"LogOut"
                                                                   message:@"Do you want to logout from this application?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* proceedAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              
               [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"logged_in"];
               self.tabBarController.selectedIndex = 0;
                                                          
    }];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                          handler:nil];
    
    [alert addAction:cancelAction];
    [alert addAction:proceedAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
