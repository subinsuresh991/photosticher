//
//  FirstViewController.m
//  PhotoSticher
//
//  Created by Subin on 26/09/16.
//  Copyright © 2016 qburst. All rights reserved.
//

#import "MapViewController.h"
#import "InfoWindow.h"
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/UIImageView+WebCache.h>

NSString * const kNameKey = @"name";
NSString * const kIcon = @"icon";
NSString * const kLatitudeKeypath = @"geometry.location.lat";
NSString * const kLongitudeKeypath = @"geometry.location.lng";

@interface MapViewController () {
    
    GMSMapView *mapView;
    
}

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager setDelegate:self];
    // this check is required for iOS 8+
    // selector 'requestWhenInUseAuthorization' is first introduced in iOS 8
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
       // [self.locationManager requestWhenInUseAuthorization];
       [self.locationManager requestAlwaysAuthorization];
    }
  //  [self.locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
    [self.locationManager startUpdatingLocation];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
                                                            longitude:151.20
                                                                 zoom:6];
    mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView.myLocationEnabled = YES;
    mapView.delegate = self;
    self.view = mapView;
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"]) {
        [self performSelector:@selector(displayLoginScreen)
                   withObject:nil afterDelay:0.0];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

  #pragma mark - Private methods
- (void)displayLoginScreen{
    [self performSegueWithIdentifier: @"loginSegue" sender: self];
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

  #pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLLocation *location = [locations lastObject];
    mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                    zoom:14];
    
    
    [[PlaceLoader sharedInstance] loadPOIsForLocation:location radius:1000 successHandler:^(NSDictionary *response) {
        NSLog(@"Response: %@", response);
        
        if([[response objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            NSMutableArray *markersArray = [[NSMutableArray alloc]init];
            NSArray *places = [response objectForKey:@"results"];
            
            for(int i = 0; i < places.count ; i++) {
                //5
                NSDictionary *resultsDict = [places objectAtIndex:i];
                CLLocation *location = [[CLLocation alloc] initWithLatitude:[[resultsDict valueForKeyPath:kLatitudeKeypath] floatValue] longitude:[[resultsDict valueForKeyPath:kLongitudeKeypath] floatValue]];
                
                GMSMarker *marker = [[GMSMarker alloc] init];
                marker.position = location.coordinate;
                marker.title = [resultsDict objectForKey:kNameKey];
                marker.snippet = [resultsDict objectForKey:kIcon];
                [markersArray addObject:marker];
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.locationManager stopUpdatingLocation];
                for (GMSMarker *marker in markersArray) {
                    marker.map = mapView;
                }
                
            });
            
            
        }else{
            [self showAlertWithTitle:@"Errpr" andMessage:@"API Response error"];
        }
    } errorHandler:^(NSError *error) {
        [self showAlertWithTitle:@"Errpr" andMessage:error.localizedDescription];
    }];
    
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    
   [self showAlertWithTitle:@"Errpr" andMessage:error.localizedDescription];
}

  #pragma mark - GMSMapViewDelegate
- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    
    InfoWindow *view =  [[[NSBundle mainBundle] loadNibNamed:@"InfoWindow" owner:self options:nil] objectAtIndex:0];
    view.nameLabel.text = marker.title;
    [view.imageView sd_setImageWithURL:[NSURL URLWithString:marker.snippet]
                      placeholderImage:[UIImage imageNamed:@"location"]];
    return view;
}


@end
