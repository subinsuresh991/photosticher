//
//  HistoryViewController.m
//  PhotoSticher
//
//  Created by Subin on 26/09/16.
//  Copyright © 2016 qburst. All rights reserved.
//

#import "HistoryViewController.h"
#import "ImageWriterManager.h"
#import "CustomTableViewCell.h"
@interface HistoryViewController (){
    
    NSArray *imageList;
    NSString *documentPath;
    MWPhoto *selectedPhoto;
}


@end

@implementation HistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    documentPath = [[ImageWriterManager sharedManager] getDocumentDirectoryPath];

    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    imageList = [[ImageWriterManager sharedManager] getSavedImagesFromDocumentDirectory];
    [self.tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView datasource and delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [imageList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSString *fileName = [imageList objectAtIndex:indexPath.row];
    cell.timeLabel.text = fileName;
    cell.thumbUmageView.layer.cornerRadius = 32;
    cell.thumbUmageView.layer.masksToBounds = YES;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *path = [documentPath stringByAppendingPathComponent:fileName];
        NSData *data = [NSData dataWithContentsOfFile:path];
        UIImage *image = [UIImage imageWithData:data];
        image = [self imageWithImage:image scaledToSize:CGSizeMake(64, 64)];
        
        dispatch_async(dispatch_get_main_queue(), ^{
        [cell.thumbUmageView setImage:image];
        });
        
    });
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *fileName = [imageList objectAtIndex:indexPath.row];
    NSString *path = [documentPath stringByAppendingPathComponent:fileName];
    // Create array of MWPhoto objects
    selectedPhoto = [MWPhoto photoWithURL:[NSURL fileURLWithPath:path]];
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
  
    [self.navigationController pushViewController:browser animated:YES];
    
}

#pragma mark - MWPhotoBrowser datasource and delegates
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return 1;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
        return selectedPhoto;
}

#pragma mark - Private Methods
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
