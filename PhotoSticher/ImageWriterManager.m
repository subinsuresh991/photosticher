//
//  ImageWriterManager.m
//  PhotoSticher
//
//  Created by Subin on 27/09/16.
//  Copyright © 2016 qburst. All rights reserved.
//

#import "ImageWriterManager.h"


@implementation ImageWriterManager

+ (id)sharedManager {
    
    static ImageWriterManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (void)addImageToDocumentDirectory:(UIImage *)image{
    
    NSData *pngData = UIImagePNGRepresentation(image);
    
    NSString *documentsPath = [self getDocumentDirectoryPath]; //Get the docs directory
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd_MMM_YYYY_HH_mm_SS"];
    
    NSDate *now = [NSDate date];
    NSString *theDate = [dateFormat stringFromDate:now];
    NSString *fileName = [theDate stringByAppendingString:@".png"];
    
    NSString *path = [documentsPath stringByAppendingPathComponent:fileName];
    
    [pngData writeToFile:path atomically:YES];
    
}

- (NSArray *)getSavedImagesFromDocumentDirectory{
    
    NSString *documentsPath = [self getDocumentDirectoryPath];
    NSArray *filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsPath  error:nil];
    return filePathsArray;
}

- (NSString *)getDocumentDirectoryPath{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    return documentsPath;
}

@end
