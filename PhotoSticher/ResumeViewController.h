//
//  ResumeViewController.h
//  PhotoSticher
//
//  Created by Subin on 26/09/16.
//  Copyright © 2016 qburst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResumeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
