//
//  CustomTableViewCell.m
//  PhotoSticher
//
//  Created by Subin on 27/09/16.
//  Copyright © 2016 qburst. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
