//
//  PlaceLoader.m
//  PhotoSticher
//
//  Created by Subin on 26/09/16.
//  Copyright © 2016 qburst. All rights reserved.
///

#import "PlaceLoader.h"

//1
#import <CoreLocation/CoreLocation.h>
#import <Foundation/NSJSONSerialization.h>
//2
NSString * const apiURL = @"https://maps.googleapis.com/maps/api/place/";
//NSString * const apiKey = @"AIzaSyAOlckmqcle6FD8Du1BslpMXvLkgby0nsI";
NSString * const apiKey = @"AIzaSyBiboaUo016F3YBqRn-9DdbdX6jVvDDRIc";
//NSString * const apiKey = @"AIzaSyCfD-AIzaSyB7ZbHGfKFdPmcLtGKs67kd56j4uouIOHs";
//AIzaSyCs_MMHSOjw8wvcZFrPJJsaKNywUuy39aY

//3
@interface PlaceLoader ()

@property (nonatomic, strong) SuccessHandler successHandler;
@property (nonatomic, strong) ErrorHandler errorHandler;
@property (nonatomic, strong) NSMutableData *responseData;

@end

@implementation PlaceLoader

+ (PlaceLoader *)sharedInstance{
   
    static PlaceLoader *instance = nil;
    static dispatch_once_t onceToken;
    
    //2
    dispatch_once(&onceToken, ^{
        instance = [[PlaceLoader alloc] init];
    });
    
    //3
    return instance;
}

- (void)loadPOIsForLocation:(CLLocation *)location radius:(int)radius successHandler:(SuccessHandler)handler errorHandler:(ErrorHandler)errorHandler {
    //1
    _responseData = nil;
    [self setSuccessHandler:handler];
    [self setErrorHandler:errorHandler];
    
    //2
    CLLocationDegrees latitude = [location coordinate].latitude;
    CLLocationDegrees longitude = [location coordinate].longitude;
    
    //3
    NSMutableString *uri = [NSMutableString stringWithString:apiURL];
    [uri appendFormat:@"nearbysearch/json?location=%f,%f&radius=%d&sensor=true&types=establishment&key=%@", latitude, longitude, radius, apiKey];
    
    //7
    //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
  //  NSLog(@"Starting connection: %@ for request: %@", connection, request);
    
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:uri]];
    request.HTTPMethod = @"GET";
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSError *error = nil;
    
    
    if (!error) {
        
        NSURLSessionDataTask *downloadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    
                    
                    NSDictionary* json = [NSJSONSerialization
                                          JSONObjectWithData:data
                                          options:kNilOptions
                                          error:&error];
                    if(_successHandler) {
                        _successHandler(json);
                    }
                    
                    
                }else{
                    if(_errorHandler) {
                        _errorHandler(error);
                    }
                }
            }else{
                if(_errorHandler) {
                    _errorHandler(error);
                }
            }
            
        }];
        
        
        [downloadTask resume];
        
    }
}

//- (void)loadDetailInformation:(NSString *)reference successHanlder:(SuccessHandler)handler errorHandler:(ErrorHandler)errorHandler {
//    _responseData = nil;
//    _successHandler = handler;
//    _errorHandler = errorHandler;
//    
//    NSMutableString *uri = [NSMutableString stringWithString:apiURL];
//    
//    [uri appendFormat:@"details/json?reference=%@&sensor=true&key=%@",reference, apiKey];
//    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[uri stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20.0f];
//    
//    [request setHTTPShouldHandleCookies:YES];
//    [request setHTTPMethod:@"GET"];
//    
//    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//    
//    NSLog(@"Starting connection: %@ for request: %@", connection, request);
//}
@end
