//
//  HistoryViewController.h
//  PhotoSticher
//
//  Created by Subin on 26/09/16.
//  Copyright © 2016 qburst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"


@interface HistoryViewController : UIViewController<MWPhotoBrowserDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
