//
//  FirstViewController.h
//  PhotoSticher
//
//  Created by Subin on 26/09/16.
//  Copyright © 2016 qburst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "PlaceLoader.h"
#import <GoogleMaps/GoogleMaps.h>

@interface MapViewController : UIViewController<CLLocationManagerDelegate,GMSMapViewDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSArray *locations;


@end

