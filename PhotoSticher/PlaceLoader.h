//
//  PlaceLoader.h
//  PhotoSticher
//
//  Created by Subin on 26/09/16.
//  Copyright © 2016 qburst. All rights reserved.
//

#import <Foundation/Foundation.h>


@class CLLocation;
@class Place;

typedef void (^SuccessHandler)(NSDictionary *responseDict);
typedef void (^ErrorHandler)(NSError *error);

@interface PlaceLoader : NSObject


+ (PlaceLoader *)sharedInstance;
- (void)loadPOIsForLocation:(CLLocation *)location radius:(int)radius successHandler:(SuccessHandler)handler errorHandler:(ErrorHandler)errorHandler;

@end
