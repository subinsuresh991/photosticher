//
//  ImageWriterManager.h
//  PhotoSticher
//
//  Created by Subin on 27/09/16.
//  Copyright © 2016 qburst. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageWriterManager : NSObject

+ (id)sharedManager;
- (void)addImageToDocumentDirectory:(UIImage *)image;
- (NSArray *)getSavedImagesFromDocumentDirectory;
- (NSString *)getDocumentDirectoryPath;
@end
