//
//  CustomTableViewCell.h
//  PhotoSticher
//
//  Created by Subin on 27/09/16.
//  Copyright © 2016 qburst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbUmageView;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@end
